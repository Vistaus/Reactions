const c = @import("c.zig");
const std = @import("std");

pub const ImageFormat = enum {
    mp4,
    gif,

    pub fn register() c.GType {
        return c.g_enum_register_static("ImageFormat", &values);
    }
};

const value_mp4 = c.GEnumValue{.value = @enumToInt(ImageFormat.mp4), .value_name = "mp4", .value_nick = "MP4"};
const value_gif = c.GEnumValue{.value = @enumToInt(ImageFormat.gif), .value_name = "gif", .value_nick = "GIF"};
//const value_zeroes = std.mem.zeroes(c.GEnumValue); Compiler crash?! TODO Investigate
const value_zeroes = c.GEnumValue{.value = 0, .value_name = null, .value_nick = null};

pub const values = [_]c.GEnumValue {
    value_mp4,
    value_gif,
    value_zeroes};


