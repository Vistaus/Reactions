const Builder = @import("std").build.Builder;

pub fn build(b: *Builder) void {
    // Standard target options allows the person running `zig build` to choose
    // what target to build for. Here we do not override the defaults, which
    // means any target is allowed, and the default is native. Other options
    // for restricting supported target set are available.
    const target = b.standardTargetOptions(.{});

    // Standard release options allow the person running `zig build` to select
    // between Debug, ReleaseSafe, ReleaseFast, and ReleaseSmall.
    const mode = b.standardReleaseOptions();

    const exe = b.addExecutable("reactions", "src/main.zig");
    exe.setTarget(target);
    exe.setBuildMode(mode);

    exe.addIncludeDir("/usr/include");
    exe.addIncludeDir("/usr/include/x86_64-linux-gnu");
    exe.addIncludeDir("/usr/include/aarch64-linux-gnu");
    exe.addLibPath("/usr/lib");
    exe.addLibPath("/usr/lib/x86_64-linux-gnu");
    exe.addLibPath("/usr/lib/aarch64-linux-gnu");

    exe.linkLibC();
    exe.linkSystemLibrary("c");
    exe.linkSystemLibrary("gtk4");
    exe.linkSystemLibrary("libadwaita-1");
    exe.linkSystemLibrary("libsoup-2.4");

    exe.setOutputDir(".");
    exe.install();

    const run_cmd = exe.run();
    run_cmd.step.dependOn(b.getInstallStep());
    if (b.args) |args| {
        run_cmd.addArgs(args);
    }

    const run_step = b.step("run", "Run the app");
    run_step.dependOn(&run_cmd.step);
}
