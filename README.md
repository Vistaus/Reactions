# Reactions

GIF search for GNOME desktop and mobile devices.

Reactions is written in Zig and uses GTK4 and libadwaita. It uses the GIPHY search API, but is intended to support multiple API providers.

![a gif playing in the Reactions app](example.gif)

## Building
It's easiest to build the project with `flatpak-builder`, which will handle all the dependencies. Otherwise, you can build directly with `zig build`, just check the flatpak manifest .json for the list of dependencies.

## Background
Reactions is a proof-of-concept for me to try working with C libraries in Zig, and also to try out GTK4. Hopefully it's a fun little app as well!
